package com.example

import io.circe._, io.circe.generic.auto._, io.circe.parser._, io.circe.syntax._

case class CastMember(
                       cast_id: Option[Int],
                       character: String,
                       credit_id: String,
                       gender: String,
                       id: String,
                       name: String,
                       order: String,
                       profile_path: String
                     )

object CirceTest {

  def main(args: Array[String]): Unit = {
    //val str = """{"cast_id": 21, "character": "President René Jean D"Astier", "credit_id": "52fe44dac3a36847f80adfd5", "gender": 2, "id": 43115, "name": "Clement von Franckenstein", "order": 14, "profile_path": "/pwpD8AxmseZOmnWXRsDErKk08Ro.jpg"}"""
   // val str = """{ "cast_id": 21, "character": "President René Jean D"Astier", "credit_id": "52fe44dac3a36847f80adfd5", "gender": 2 }"""

    val str1 = """{'cast_id': 10, 'character': "Themroc's Sister", 'credit_id': '52fe4470c3a36847f8096031', 'gender': 1, 'id': 28611, 'name': 'Béatrice Romand', 'order': 2, 'profile_path': '/44XNrVHZWwgoCWDdWGM7Km5Tjkh.jpg'}"""
    val str2 = """{'cast_id': 9, 'character': '(segment "Cinéma de Boulevard") (archive footage)', 'credit_id': '52fe45cc9251416c9103ed4d', 'gender': 2, 'id': 30181, 'name': 'Fred Astaire', 'order': 8, 'profile_path': '/tFdgseH9plbGN8XBhBcG9lscfaY.jpg'}"""

    val str = str2

    val singleQuotePairPattern = """.*'character': '(.*)', 'credit_id'.*""".r

    val singleQuoteTokenizedString = str match {
      case singleQuotePairPattern(unescaped) => {
        val escaped = unescaped.replaceAll(""""""", """@`@""")
        str.replace(unescaped, escaped)
      }
      case _ => {
        str
      }
    }

    val doubleQuotePairPattern = """.*'character': "(.*)", 'credit_id'.*""".r

    val doubleQuoteTokenizedString = singleQuoteTokenizedString match {
      case doubleQuotePairPattern(unescaped) => {
        val escaped = unescaped.replaceAll("""'""", """@`@""")
        singleQuoteTokenizedString.replace(unescaped, escaped)
      }
      case _ => singleQuoteTokenizedString
    }

    val doubleQuotedString = doubleQuoteTokenizedString
      .replaceAll("\'", "\"")
      .replaceAll("None", "null")

    val cleanedString = doubleQuotedString.replaceAll("""@`@""", """\'""")

    println(cleanedString)


    val parseResult = parse(cleanedString)

    val decodeResult = parseResult match {
      case Left(e) => e.printStackTrace
      case Right(json) => println(json)//json.as[CastMember]
    }

//    decodeResult match {
//      case Left(e) => println(e.toString)
//      case Right(castMember) => println(s"success: $castMember")
//    }

  }
}
