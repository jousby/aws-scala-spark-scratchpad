package com.example

import org.apache.spark.sql.types.{LongType, StringType, StructType}
import org.apache.spark.sql.{DataFrame, Dataset, SparkSession}


case class Person(name: String, age: Option[Long])

object JsonTest {

  def main(args: Array[String]): Unit = {

    val spark = SparkSession
      .builder
      .master("local")
      .appName("JsonLoader")
      .getOrCreate

    import spark.implicits._

    val json = List(
      """{"name": "James", "age": 40}""",
      """{"name": "Ryan", "age": 2}""",
      """{"name": "Ethan", "age": 5}""",
      """{"name": "Lin", "age": "WHAT"}"""
    )

    val jsonDS: Dataset[String] = spark.createDataset(json)

    val personSchema = (new StructType)
      .add("name", StringType)
      .add("age", StringType)

    val what = spark.read.schema(personSchema).json(jsonDS)
      .select($"name", $"age".cast(LongType))
      .as[Person]

    what.printSchema()
    what.show()
  }
}
