package com.example

import java.io.File
import java.nio.file.{FileSystems, Files, Path, Paths}
import java.time.LocalDate
import java.util

import io.circe.Decoder.Result

import collection.JavaConverters._
import org.apache.commons.io.FileUtils
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.types._
import org.apache.spark.sql.{DataFrame, Dataset, SparkSession}

import scala.util.{Failure, Success, Try}

case class MovieRaw(
                  id: Option[Int],
                  adult: Option[Boolean],
                  budget: Option[Int],
                  imdb_id: String,
                  original_language: Option[String],
                  original_title: String,
                  overview: String,
                  popularity: Option[Double],
                  poster_path: String,
                  production_companies: String,
                  production_countries: String,
                  release_date: java.sql.Date,
                  revenue: Option[Int],
                  runtime: Option[Int],
                  spoken_languages: String,
                  status: String,
                  tagline: Option[String],
                  title: String,
                  vote_average: Option[Double],
                  vote_count: Option[Int]
)


case class Movie(
  id: Int,
  budget: Option[Int],
  imdb_id: String,
  original_language: Option[String],
  original_title: String,
  overview: String,
  popularity: Option[Double],
  poster_path: String,
  production_companies: String,
  production_countries: String,
  release_date: java.sql.Date,
  revenue: Option[Int],
  runtime: Option[Int],
  spoken_languages: String,
  status: String,
  tagline: Option[String],
  title: Option[String],
  vote_average: Option[Double],
  vote_count: Option[Int]
)

//    adult	belongs_to_collection	budget	genres	homepage	id	imdb_id	original_language	original_title	overview	popularity
// poster_path	production_companies	production_countries	release_date	revenue	runtime	spoken_languages	status	tagline
// title	video	vote_average	vote_count

//      FALSE
// {'id': 10194, 'name': 'Toy Story Collection', 'poster_path': '/7G9915LfUQ2lVfwMEEhDsn3kT4B.jpg', 'backdrop_path': '/9FBwqcd9IRruEDUrTdcaafOMKUq.jpg'}
// 30000000
// [{'id': 16, 'name': 'Animation'}, {'id': 35, 'name': 'Comedy'}, {'id': 10751, 'name': 'Family'}]
// http://toystory.disney.com/toy-story
// 862
// tt0114709
// en
// Toy Story
// Led by Woody, Andy's toys live happily in his room until Andy's birthday brings Buzz Lightyear onto the scene. Afraid of losing his
// place in Andy's heart, Woody plots against Buzz. But when circumstances separate Buzz and Woody from their owner, the duo eventually learns to
// put aside their differences.
// 21.946943
// /rhIRbceoE9lR4veEXuwCC2wARtG.jpg
// [{'name': 'Pixar Animation Studios', 'id': 3}]
// [{'iso_3166_1': 'US', 'name': 'United States of America'}]
// 10/30/95
// 373554033
// 81
// [{'iso_639_1': 'en', 'name': 'English'}]
// Released
//
// Toy Story
// FALSE
// 7.7
// 5415

case class CreditsRaw(
                       cast: String,
                       crew: String,
                       id: String
                     )

//      [{'cast_id': 14, 'character': 'Woody (voice)', 'credit_id': '52fe4284c3a36847f8024f95', 'gender': 2,
//    'id': 31, 'name': 'Tom Hanks', 'order': 0, 'profile_path': '/pQFoyx7rp09CJTAb932F2g8Nlho.jpg'},
case class CastMember(
                       cast_id: Int,
                       character: String,
                       credit_id: String,
                       gender: Int,
                       id: Int,
                       name: String,
                       order: Int,
                       profile_path: Option[String]
                     )

case class CrewMember(
                       credit_id: String,
                       department: String,
                       gender: Int,
                       id: Int,
                       job: String,
                       name: String,
                       profile_path: Option[String]
                     )

case class Credits(
                    cast: List[CastMember],
                    crew: List[CrewMember],
                    id: String
                  )

object SparkTransfer {

  def main(args: Array[String]): Unit = {

    val spark = SparkSession
      .builder
      .master("local")
      .appName("MoviesDataLoader")
      .getOrCreate

    import spark.implicits._


//    val schema = (new StructType)
//      .add("id", IntegerType)
//
//    val movies: Dataset[MovieRaw] = spark
//      .read
//      //.schema(schema)
//      .option("header", true)
//      .option("quote", "\"")
//      .option("escape", "\"")
//      .csv("/Users/joousby/Downloads/the-movies-dataset/movies_metadata.csv")
//      .select(
//        $"id".cast(IntegerType),
//        $"adult",
//        $"budget".cast(IntegerType),
//        $"imdb_id",
//        $"original_language",
//        $"original_title",
//        $"overview",
//        $"popularity".cast(DoubleType),
//        $"poster_path",
//        $"production_companies",
//        $"production_countries",
//        $"release_date".cast(DateType),
//        $"revenue".cast(IntegerType),
//        $"runtime".cast(IntegerType),
//        $"spoken_languages",
//        $"status",
//        $"tagline",
//        $"title",
//        $"vote_average".cast(DoubleType),
//        $"vote_count".cast(IntegerType))
//      .as[MovieRaw]
//
//    movies.printSchema()
//
//
//    movies.show(10)
//
//    val filteredMovies = movies
//      .filter(!_.adult.getOrElse(true))
//      .filter(_.id.isDefined)
//      .map(raw => {
//        Movie(
//          raw.id.get,
//          raw.budget,
//          raw.imdb_id,
//          raw.original_language,
//          raw.original_title,
//          raw.overview,
//          raw.popularity,
//          raw.poster_path,
//          raw.production_companies,
//          raw.production_countries,
//          raw.release_date,
//          raw.revenue,
//          raw.runtime,
//          raw.spoken_languages,
//          raw.status,
//          raw.tagline,
//          raw.title,
//          raw.vote_average,
//          raw.vote_count
//        )
//      })
//
//
//    println(filteredMovies.count)

//    val parquetFileName = "/Users/joousby/Downloads/the-movies-dataset/movies_metadata.parquet"
//    val parquetFile = new File(parquetFileName)
//    FileUtils.deleteDirectory(parquetFile)
//
//    filteredMovies.write.parquet(parquetFileName)

    val movies: Dataset[Movie] = spark
      .read
      .parquet("/Users/joousby/Downloads/the-movies-dataset/movies_metadata.parquet")
      //.parquet("s3a://jodataprodone-datasets/movies/movies_metadata.parquet")
      .as[Movie]

    movies.createOrReplaceTempView("movies")

    movies.filter(_.title.contains("Star Wars")).show

    // filter out the movies that don't have clean data or weren't profitable
    val filteredMovies = movies
      .filter(_.budget.isDefined)                   // drop movies where budget is missing
      .filter(_.revenue.isDefined)                  // drop movies where revenue is missing
      .filter(_.budget.get > 5000)                  // drop movies where budget is less than 5k
      .filter(_.revenue.get > 0)                    // drop movies where revenue is less than 0
      .filter(m => m.revenue.get > m.budget.get)    // drop movies that weren't profitable

    // define your complex business logic (this is obviously not complex but you can see how you can extend this)
    def profitMultipleFn(revenue: Int, budget: Int) = revenue / budget

    // run our function over each of the movies in the filtered movie set
    // in this case we are building a new result type that just has the fields we are interested in.
    val result = filteredMovies.map(m => {
      (m.title.getOrElse(""), m.budget, m.revenue, profitMultipleFn(m.revenue.get, m.budget.get))
    })

    // sort the results by the profit multiple field and show the first value
    result.orderBy($"_4".desc).show(1)



//    val creditsRaw: Dataset[CreditsRaw] = spark
//      .read
//      .option("header", true)
//      .option("quote", "\"")
//      .option("escape", "\"")
//      .csv("/Users/joousby/Downloads/the-movies-dataset/credits.csv")
//      .as[CreditsRaw]
//
//    def unsafeJsonArraySplit(arr: String): Option[Array[String]] = {
//      if (arr.length > 4) {
//        Some(arr
//          .drop(1)
//          .dropRight(2)
//          .split("},")
//          .map(_ + "}"))
//      }
//      else {
//        None
//      }
//    }
//
//    def cleanString(str: String): String = {
//
//      var cleanedString = str
//      val singleQuotePairPattern = """.*'character': '(.*)', 'credit_id'.*""".r
//
//      cleanedString = cleanedString match {
//        case singleQuotePairPattern(unescaped) => {
//          val escaped = unescaped.replaceAll(""""""", """@`@""")
//          cleanedString.replace(unescaped, escaped)
//        }
//        case _ => cleanedString
//      }
//
//      val singleQuotePairPattern2 = """.*'name': '(.*)',.*""".r
//
//      cleanedString = cleanedString match {
//        case singleQuotePairPattern2(unescaped) => {
//          val escaped = unescaped.replaceAll(""""""", """@`@""")
//          cleanedString.replace(unescaped, escaped)
//        }
//        case _ => cleanedString
//      }
//
//      val singleQuotePairPattern3 = """.*'job': '(.*)',.*""".r
//
//      cleanedString = cleanedString match {
//        case singleQuotePairPattern3(unescaped) => {
//          val escaped = unescaped.replaceAll(""""""", """@`@""")
//          cleanedString.replace(unescaped, escaped)
//        }
//        case _ => cleanedString
//      }
//
//      val doubleQuotePairPattern = """.*'character': "(.*)", 'credit_id'.*""".r
//
//      cleanedString = cleanedString match {
//        case doubleQuotePairPattern(unescaped) => {
//          val escaped = unescaped.replaceAll("""'""", """@`@""")
//          cleanedString.replace(unescaped, escaped)
//        }
//        case _ => cleanedString
//      }
//
//      val doubleQuotePairPattern2 = """.*'name': "(.*)",.*""".r
//
//      cleanedString = cleanedString match {
//        case doubleQuotePairPattern2(unescaped) => {
//          val escaped = unescaped.replaceAll("""'""", """@`@""")
//          cleanedString.replace(unescaped, escaped)
//        }
//        case _ => cleanedString
//      }
//
//      val doubleQuotePairPattern3 = """.*'job': "(.*)",.*""".r
//
//      cleanedString = cleanedString match {
//        case doubleQuotePairPattern3(unescaped) => {
//          val escaped = unescaped.replaceAll("""'""", """@`@""")
//          cleanedString.replace(unescaped, escaped)
//        }
//        case _ => cleanedString
//      }
//
//      cleanedString = cleanedString
//        .replaceAll("\'", "\"")
//        .replaceAll("""\\x92""", """'""")
//        .replaceAll("""\\xa0""", """ """)
//        .replaceAll("""\\xad""", """ """)
//        .replaceAll("None", "null")
//
//      cleanedString = cleanedString.replaceAll("""@`@""", """\'""")
//
//      cleanedString
//    }
//
//    import io.circe._, io.circe.generic.auto._, io.circe.parser._, io.circe.syntax._
//
//    val credits = creditsRaw.map(raw => {
//      val castStrings = unsafeJsonArraySplit(raw.cast)
//      val cast: List[CastMember] = castStrings match {
//        case Some(strings) => {
//          strings.map { s =>
//            val cleanedString = cleanString(s)
//
//            val parseResult: Either[ParsingFailure, Json] = parse(cleanedString)
//            parseResult match {
//              case Left(e) => {
//                println(s"parse failure \n $cleanedString \n $s \n ${e.message} ${e.underlying}")
//                None
//              }
//              case Right(json) => {
//                val decodeResult = json.as[CastMember]
//                decodeResult match {
//                  case Left(e) => {
//                    println(s"decode failure = ${e.message} $cleanedString")
//                    None
//                  }
//                  case Right(castMember) => Some(castMember)
//                }
//              }
//
//            }
//          }
//          .filter(_.isDefined)
//          .map(_.get)
//          .toList
//        }
//        case None => {
//          List.empty[CastMember]
//        }
//      }
//
//      val crewStrings = unsafeJsonArraySplit(raw.crew)
//      val crew: List[CrewMember] = crewStrings match {
//        case Some(strings) => {
//          strings.map { s =>
//            val cleanedString = cleanString(s)
//
//            val parseResult: Either[ParsingFailure, Json] = parse(cleanedString)
//            parseResult match {
//              case Left(e) => {
//                println(s"parse failure \n $cleanedString \n $s \n ${e.message} ${e.underlying}")
//                None
//              }
//              case Right(json) => {
//                val decodeResult = json.as[CrewMember]
//                decodeResult match {
//                  case Left(e) => {
//                    println(s"decode failure = ${e.message} $cleanedString")
//                    None
//                  }
//                  case Right(crewMember) => Some(crewMember)
//                }
//              }
//
//            }
//          }
//          .filter(_.isDefined)
//          .map(_.get)
//          .toList
//        }
//        case None => {
//          List.empty[CrewMember]
//        }
//      }
//
//      Credits(cast, crew, raw.id)
//    })
//
//    println(credits.count)
//
//    credits.show(10)
//
//    val creditParquetFileName = "/Users/joousby/Downloads/the-movies-dataset/credits.parquet"
//    val creditParquetFile = new File(creditParquetFileName)
//    FileUtils.deleteDirectory(creditParquetFile)
//
//    credits.write.parquet(creditParquetFileName)
  }
}
