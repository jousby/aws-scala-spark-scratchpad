package com.example

import com.amazonaws.auth.{AWSSessionCredentials, AWSSessionCredentialsProvider, AWSStaticCredentialsProvider, BasicSessionCredentials}
import com.amazonaws.auth.profile.ProfileCredentialsProvider
import com.amazonaws.regions.Regions
import com.amazonaws.services.securitytoken.AWSSecurityTokenServiceClientBuilder
import com.amazonaws.services.securitytoken.model.AssumeRoleRequest
import com.amazonaws.services.translate.AmazonTranslateClientBuilder
import com.amazonaws.services.translate.model.TranslateTextRequest

object TranslateExperiment extends App {

  // From the calling account get a handle on the sts service
  val sts = AWSSecurityTokenServiceClientBuilder
    .standard()
    .withRegion(Regions.US_EAST_1)
    .withCredentials(new ProfileCredentialsProvider("appdevone-devman"))
    .build()

  // Get temp credentials to assume the IAGTranslatePreviewAcccess role in the AWS preview account
  val roleReq = new AssumeRoleRequest()
    .withRoleArn("arn:aws:iam::784386253165:role/IAGTranslatePreviewAccess")
    .withDurationSeconds(3600)
    .withRoleSessionName("translate")

  val res = sts.assumeRole(roleReq)
  val stsCredentials = res.getCredentials

  val basicCredentials = new BasicSessionCredentials(
    stsCredentials.getAccessKeyId,
    stsCredentials.getSecretAccessKey,
    stsCredentials.getSessionToken)

  // Get a handle on the translate client in the AWS preview account using the temp credentials
  val translate = AmazonTranslateClientBuilder
    .standard()
    .withRegion(Regions.US_EAST_1)
    .withCredentials(new AWSStaticCredentialsProvider(basicCredentials))
    .build()

  // Run your translation request
  val req = new TranslateTextRequest()
    .withSourceLanguageCode("en") // english
    .withTargetLanguageCode("zh") // chinese
    .withText("No Gong Gong, my 5 year old son doesn't need three ice creams in one day.")

  val resp = translate.translateText(req)

  println(resp.getTranslatedText)
}
