package com.example

import java.io.ByteArrayInputStream

import com.amazonaws.auth.profile.ProfileCredentialsProvider
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import com.amazonaws.services.s3.model.ObjectMetadata
import com.google.cloud.storage.Storage.BlobListOption
import com.google.cloud.storage.{Blob, Bucket, StorageOptions}

import scala.collection.JavaConverters._

object GoogleStorageToS3 {

  def main(args: Array[String]): Unit = {

    // Google Storage client
    val googleStorage = StorageOptions.getDefaultInstance.getService

    // S3 client
    val s3 = AmazonS3ClientBuilder.standard().build()

    // Google Bucket handle
    val googleBucket: Bucket = googleStorage.get("jo-bitcoin")

    // List of files in my Google bucket
    val blockFiles: List[Blob] = googleBucket.list(BlobListOption.prefix("blocks")).iterateAll.asScala.toList
    //val transactionFiles: List[Blob] = googleBucket.list(BlobListOption.prefix("transactions")).iterateAll.asScala.toList

    var remaining = blockFiles.size

    // Parallel copy of Google Storage files to S3
    blockFiles.par.foreach { file =>
      val fileName = file.getName

      // Read blob from google
      val content = googleStorage.readAllBytes(file.getBlobId)

      // Push to S3
      val metadata = new ObjectMetadata()
      metadata.setContentLength(content.length)
      val contentStream = new ByteArrayInputStream(content)
      s3.putObject("jodataprodone-datasets", "bitcoin/" + fileName, contentStream, metadata)
      contentStream.close()

      remaining-=1
      println(s"Finished S3 put on file: bitcoin/$fileName ($remaining files remaining)")
    }
  }
}
