# Apache Spark on Amazon EMR Demo

## Brief

On a live Apache Spark cluster that is created and managed by Amazon EMR (Elastic Map Reduce) you will do some
data exploration and analytics on a Movies dataset containing over 45,000 movies. Using Apache Zeppelin (a web based
notebook for doing data analytics) you will use Scala / SQL / Python or another of Zeppelins supported languages to
load and then analyse the dataset.

Amazon EMR gives you a push button wizard for launching a cluster of ec2 instances and installing both Spark
and Zeppelin (among other frameworks).

<TODO insert picture of cluster>

## Getting Started

### Launching the notebook
> The lab notebook contains both further instructions and the lab exercises. You can access the lab at 
the following url:

> [http://ec2-34-201-245-212.compute-1.amazonaws.com:8890/#/notebook/2DC5ZRYBP](http://ec2-34-201-245-212.compute-1.amazonaws.com:8890/#/notebook/2DC5ZRYBP)
