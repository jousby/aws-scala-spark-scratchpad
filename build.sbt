// The file <project root>/project/Dependencies.scala contains a full list of all the AWS apis you can use in your
// libraryDependencies section below. You can also update the version of the AWS libs in this file as well.
import Dependencies._


// Project definition
lazy val root = project
  .in(file("."))
  .settings(
    inThisBuild(List(
      name := "awsAkkaQuickstart",
      organization := "com.example",
      scalaVersion := "2.11.12",
      version      := "0.1.0-SNAPSHOT",
      scalacOptions ++= Seq("-unchecked", "-deprecation"),
    )),
    libraryDependencies ++= Seq(
      akkaActor,
      akkaSlf4j,
      apacheSpark,
      awsS3,
      awsSts,
      awsTranslate,
      circeCore,
      circeGeneric,
      circeParser,
      googleStorage,
      logback,
      akkaTestKit % Test,
      scalaMock % Test,
      scalaTest % Test
    )
  )
